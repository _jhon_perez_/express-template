import express from "express";
import httpStatus from "http-status";
import { inject } from "inversify";
import {
	BaseHttpController,
	controller,
	httpGet,
	interfaces,
	request,
	response,
} from "inversify-express-utils";

import { UserFindAll } from "../../../../../application/services/find/UserFindAll";
import Types from "../../../../configuration/dependency-injection/Types";

@controller("/users")
export class UserFindAllController extends BaseHttpController implements interfaces.Controller {
	constructor(@inject(Types.UserFindAll) private readonly userService: UserFindAll) {
		super();
	}

	@httpGet("/")
	private async findAll(@request() _req: express.Request, @response() _res: express.Response) {
		try {
			const data = await this.userService.run();

			return this.json(data, httpStatus.OK);
		} catch (error) {
			return this.json(error, httpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
